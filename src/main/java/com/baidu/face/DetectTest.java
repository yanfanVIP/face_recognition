package com.baidu.face;

import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONObject;
import com.baidu.Config;
import com.baidu.aip.face.AipFace;
import com.baidu.aip.face.MatchRequest;
import com.baidu.util.ImageBase64;

public class DetectTest {
	
	public static void main(String[] args) throws IOException {
		match();
	}
	
	public static void detect() throws IOException {
		AipFace aipFace = new AipFace(Config.APPID, Config.APIKEY, Config.SECRETKEY);
		aipFace.setHttpProxy("ssfirewall", 8080);
		JSONObject data = aipFace.detect(ImageBase64.getBase64ImageFromPath("E:\\photo\\face\\5.jpg"), "BASE64", null);
		System.out.println(data);
	}
	
	public static void match() throws IOException {
		AipFace aipFace = new AipFace(Config.APPID, Config.APIKEY, Config.SECRETKEY);
		aipFace.setHttpProxy("ssfirewall", 8080);
		ArrayList<MatchRequest> requests = new ArrayList<MatchRequest>();
		requests.add(new MatchRequest(ImageBase64.getBase64ImageFromPath("E:\\photo\\face\\1.jpg"), "BASE64"));
		requests.add(new MatchRequest(ImageBase64.getBase64ImageFromPath("E:\\photo\\face\\6.jpg"), "BASE64"));
		JSONObject data = aipFace.match(requests);
		System.out.println(data);
	}
}
